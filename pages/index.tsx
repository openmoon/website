import ContentSection from "../components/contentSection";
import Footer from "../components/footer";
import Nav from "../components/nav";
import Stars from "../components/stars";

export default function Home() {
    return (
        <div className="bg-indigo-900">
            <Nav isLanding={true} currentId="home"></Nav>
            <main>
                <Stars />
                <div className="flex flex-col">
                    <ContentSection title="The Token" bgColor="indigo-900">
                        <p className="sm:text-lg">
                            <b>OPMN</b> token address is{" "}
                            <b>0xd8b9f9b8dd11b9f6a1e0c7da6a2690720fdc8da3</b>
                        </p>
                        <p className="sm:text-lg">
                            To get <b>OPMN</b>:
                            <ul className="ml-5 list-disc">
                                <li>
                                    <a
                                        href="https://app.pangolin.exchange/#/swap?inputCurrency=avax&outputCurrency=0xD8b9F9B8dd11B9F6a1e0C7Da6a2690720FDC8Da3"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="underline"
                                    >
                                        Trade on Pangolin
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="https://www.traderjoexyz.com/#/trade?inputCurrency=avax&outputCurrency=0xD8b9F9B8dd11B9F6a1e0C7Da6a2690720FDC8Da3"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="underline"
                                    >
                                        Trade on TraderJoe
                                    </a>
                                </li>
                            </ul>
                        </p>
                    </ContentSection>

                    <ContentSection
                        title="What is the OpenMoon token?"
                        bgColor="indigo-700"
                    >
                        <p className="sm:text-lg">
                            <b>OPMN</b> is a deflationary, reflective,
                            auto-liquidity-adding token with unique and
                            innovative features and improvements including:
                            <ul className="ml-5 list-disc">
                                <li>Decentralization</li>
                                <li>LP-rewarded staking</li>
                                <li>Optimized liquidity-adding algorithm</li>
                            </ul>
                            To find out more details, read our{" "}
                            <a href="/whitepaper.pdf">
                                {" "}
                                <b>whitepaper</b>{" "}
                            </a>
                            !
                        </p>
                    </ContentSection>
                    <ContentSection title="Trustlessness" bgColor="indigo-900">
                        <p className="sm:text-lg">
                            The platform is built to run 100% trustlessly -
                            without any kind of privileged users. The{" "}
                            <b>OpenMoon</b> Team does not have any
                            administrative access to the token after deployment
                            of Presale Contract.
                        </p>
                    </ContentSection>
                    <ContentSection
                        title="Decentralization"
                        bgColor="indigo-700"
                    >
                        <p className="sm:text-lg">
                            Unlike most of automatic liquidity adding tokens,
                            <b>OpenMoon</b> is not tied to any particular
                            liquidity pool nor DEX. The liquidity made from
                            resources taken from transaction fee is automaticaly
                            added to whichever pool the user is currently
                            trading with.
                        </p>
                        <p className="mt-5 sm:text-lg">
                            This approach allows multiple places of trading{" "}
                            <b>OPMN</b> grow simultanously, giving the{" "}
                            <b>OPMN</b> holders more opportunities to gain from
                            arbitrage.
                        </p>
                    </ContentSection>
                    <ContentSection title="Staking" bgColor="indigo-900">
                        <p className="sm:text-lg">
                            Unlike most similiarly-looking projects, we do not
                            want to waste any LP by burning it. We redistribute
                            it to the <b>OPMN</b> holders through staking.
                        </p>
                        <p className="mt-5 sm:text-lg">
                            Users who want to use the feature must deposit their
                            tokens to the staking pools. Every pool drops{" "}
                            <b>~4%</b> of its content to the stakers per day and
                            is invented to be mathematically fair to every
                            staker regardless of the staking time.
                        </p>
                    </ContentSection>
                    <ContentSection title="Openness" bgColor="indigo-700">
                        <p className="sm:text-lg">
                            We want the project to be driven in the spirit of
                            Open Source. We achieve that by publishing all of
                            our critical code in readable and documented form
                            under copyleft licenses on our{" "}
                            <a
                                href="https://gitlab.com/openmoon"
                                target="_blank"
                                rel="noopener noreferrer"
                                className="underline"
                            >
                                Gitlab
                            </a>
                            . It enables every investor to verify project's
                            trustworthyness.
                        </p>
                    </ContentSection>
                    <ContentSection title="Fair launch" bgColor="indigo-900">
                        <p className="sm:text-lg">
                            Following the principle of <b>trustlessness</b>,
                            100% of the tokens are allocated to fixed-price
                            presale. The team does not hold any tokens from the
                            beginning and have to participate in it like every
                            other user.
                        </p>
                    </ContentSection>
                    <ContentSection title="Fees" bgColor="indigo-700">
                        <p className="sm:text-lg">
                            During each transaction, <b>OpenMoon</b> charges
                            following fees:
                            <ul className="ml-5 list-disc">
                                <li>
                                    <b>2%</b> is burned
                                </li>
                                <li>
                                    <b>2%</b> is redistributed amongst all the
                                    holders
                                </li>
                                <li>
                                    <b>3%</b> is efficiently added as liquidity
                                    to the pool, user is currently trading with
                                </li>
                            </ul>
                        </p>
                    </ContentSection>
                    <ContentSection
                        title="Plans for the future"
                        bgColor="indigo-900"
                    >
                        <p className="sm:text-lg">
                            Opening our own DEX, which would make profits to{" "}
                            <b>OPMN</b> holders using our future-proof staking
                            mechanism.
                        </p>
                    </ContentSection>
                </div>
            </main>
            <Footer />
        </div>
    );
}
