import Footer from "../components/footer";
import Nav from "../components/nav";
import Presale from "../components/presale";
import NetworkWrapper from "../components/networkWrapper";

export default function Home() {
    return (
        <div className="bg-indigo-900">
            <Nav isLanding={false} currentId="presale"></Nav>
            <main className="min-h-screen">
                <div className="flex flex-row justify-center my-10 flex-cols-3">
                    <div></div>
                    <div className="flex flex-col text-white w-max max-w-screen-xl ">
                        <NetworkWrapper showModal={true}>
                            <Presale />
                        </NetworkWrapper>
                    </div>
                </div>
            </main>
            <Footer />
        </div>
    );
}
