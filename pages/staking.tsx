import {useContext, useEffect, useState} from "react";
import Footer from "../components/footer";
import Nav from "../components/nav";
import StakingPool, {IStakingPoolProps} from "../components/stakingPool";
import {ethers} from "ethers";
import {useWeb3React} from "@web3-react/core";
import {ERC20ABI, TokenABI, TokenAdress} from "../components/consts";
import {GlobalModalContext, IGlobalModalContext} from "../components/globalModal";
import NetworkWrapper from "../components/networkWrapper";
import {tryTransact} from "../components/util";

export default function Home() {
    const [userPools, setUserPools] = useState<string[]>()

    useEffect(() => {
        if (userPools)
            localStorage.setItem("UserPools", JSON.stringify(userPools))
    }, [userPools])

    useEffect(() => {
        if (!userPools)
            setUserPools(JSON.parse(localStorage.getItem("UserPools")) ?? [])
    }, [])

    const hardcodedPools: IStakingPoolProps[] = [
        {
            reward: '0xbE67A9673935ad4ecDfDfF397f944198Eb868996',
            name: 'Joe LP (OPMN/AVAX)'
        },
        {
            reward: '0xbBD098c7a0335D4237FbDD14b91EFEd163B29E4a',
            name: 'Pangolin LP (OPMN/AVAX)'
        }
    ]
    const {account, library} = useWeb3React();
    const pools = [...hardcodedPools, ...((userPools ?? []).map(r => ({
        reward: r, onDelete: () => {
            setUserPools(userPools.filter((R) => R != r))
        }
    })))]
    const [input, setInput] = useState<string>("")

    const tokenContract = new ethers.Contract(
        TokenAdress,
        TokenABI,
        library?.getUncheckedSigner(),
    );

    const modalContext: IGlobalModalContext = useContext(GlobalModalContext)

    const onRegister = async () => {
        if (validateInput()) {
            modalContext.pushMessage(validateInput())
            return
        }
        if (input.length == 0) {
            modalContext.pushMessage("Address empty!")
            return
        }
        const tx = tokenContract.registerLP(input)
        tryTransact(tx, modalContext.pushMessage)
    }

    const onAdd = () => {
        if (validateInput()) {
            modalContext.pushMessage(validateInput())
            return
        }
        if (input.length == 0) {
            modalContext.pushMessage("Address empty!")
            return
        }
        if (userPools && !userPools.includes(input))
            setUserPools([...userPools, input])
    }

    const handleInput = (event: any) => {
        const {value} = event.target;
        setInput(value);
    };

    const validateInput = () => {
        if (input.length == 0) {
            return null
        }
        try {
            ethers.utils.getAddress(input)
        } catch (e) {return "Invalid address";}
        return null;
    }

    return (
        <div className="bg-indigo-900">
            <Nav isLanding={false} currentId="staking"></Nav>
            <main className="min-h-screen text-white">
                <p className="m-4 text-center text-7xl">Stake OPMN for community liquidity!</p>
                <p className="m-4 text-center text-7xl">Staking pools:</p>
                <NetworkWrapper showModal={true}>
                    <div className="flex flex-wrap items-start content-start m-10">
                        {pools.map((p) => <div className="inline-block m-2 w-[28rem]"><StakingPool {...p} key={p.reward} /></div>)}
                        <div className="w-[28rem] inline-block m-2 p-4 text-2xl bg-indigo-500 rounded-md">
                            <input
                                className="w-full p-3 placeholder-indigo-900 bg-indigo-500 border-4 border-indigo-900 rounded-md focus:outline-none"
                                placeholder="Address"
                                type="text"
                                value={input}
                                onChange={handleInput}
                            />
                            <div className="">
                                <button className="p-2 text-3xl bg-indigo-900 rounded-md focus:outline-none focus:ring-none hover:underline" onClick={onAdd}>
                                    Add pool
                                </button>
                                <button className="p-2 m-2 text-3xl bg-indigo-900 rounded-md focus:outline-none focus:ring-none hover:underline" onClick={onRegister}>
                                    Register LP
                                </button>
                            </div>
                            <p className="text-red-600">{validateInput()}</p>
                        </div>
                    </div>
                </NetworkWrapper>
            </main>
            <Footer />
        </div>
    );
}
