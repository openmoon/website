import {AppProps} from "next/app";
import {DefaultSeo} from "next-seo";
import "../styles/globals.css";
import {Web3ReactProvider} from "@web3-react/core";
import {Web3Provider} from "@ethersproject/providers";
import GlobalModal from "../components/globalModal";

function getLibrary(provider: any) {
    const library = new Web3Provider(provider);
    library.pollingInterval = 12000;
    return library;
}

function MyApp({Component, pageProps}: AppProps) {
    return (
        <>
            <DefaultSeo
                openGraph={{
                    type: "website",
                    url: "https://openmoon.finance/",
                    title: "OpenMoon.finance",
                    locale: "en_US",
                    images: [
                        {
                            url: "https://openmoon.finance/logotext.png",
                        },
                    ],
                }}
                title="OpenMoon.finance"
                description="Innovative, trustless yield generating platform on Avax C-Chain"
            />
            <GlobalModal>
                <Web3ReactProvider getLibrary={getLibrary}>
                    <Component {...pageProps} />
                </Web3ReactProvider>
            </GlobalModal>
        </>
    );
}

export default MyApp;
