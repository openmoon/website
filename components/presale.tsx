import { useWeb3React } from "@web3-react/core";
import { BigNumber, ethers } from "ethers";
import { useContext, useEffect, useState } from "react";
import { PresaleABI, PresaleAdress } from "./consts";
import { bigNumberToInt, isValidNumber, printBigNumber, tryTransact } from "./util";
import Countdown from "react-countdown";
import {GlobalModalContext, IGlobalModalContext} from "./globalModal";

interface IPresaleConstants {
    hardCap: number;
    tokenPerAvax: number;
    endtime: Date;
}

export default function Presale() {
    const { account, library, chainId } = useWeb3React();
    const [presaleBalance, setPresaleBalance] = useState<BigNumber>(null);
    const [avaxBalance, setAvaxBalance] = useState<BigNumber>(null);
    const [depositAmount, setDepositAmount] = useState<string>("0");
    const [presaleConstants, setPresaleConstants] = useState<IPresaleConstants>(
        {
            hardCap: 0,
            tokenPerAvax: 0,
            endtime: null,
        },
    );

    const setState = () => {
        library.getBalance(PresaleAdress).then((b) => setPresaleBalance(b));

        library.getBalance(account).then((b) => setAvaxBalance(b));
    };

    useEffect(() => {
        if (
            !!account &&
            !!library
        ) {
            setState();
            library.on("block", setState);

            const presaleContract = new ethers.Contract(
                PresaleAdress,
                PresaleABI,
                library,
            );

            presaleContract.hardCapAvax().then((hc: BigNumber) => {
                const newState = presaleConstants;
                newState.hardCap = bigNumberToInt(hc);
                setPresaleConstants(newState);
            });

            presaleContract.OPMNPerAvax().then((tpa: BigNumber) => {
                const newState = presaleConstants;
                newState.tokenPerAvax = tpa.toNumber();
                setPresaleConstants(newState);
            });

            presaleContract.endTime().then((et: BigNumber) => {
                const newState = presaleConstants;
                newState.endtime = new Date(et.toNumber() * 1000);
                setPresaleConstants(newState);
            });
        }
    }, [account, chainId, library]);

    const modalContext: IGlobalModalContext = useContext(GlobalModalContext)

    const printPercentage = (b: BigNumber): string => {
        const bString = b.toString();
        return (
            (
                parseFloat(bString.slice(0, bString.length - 14)) /
                (presaleConstants.hardCap * 100)
            ).toPrecision(4) + "%"
        );
    };

    const handleDepositInput = (event: any) => {
        const { value } = event.target;
        if (isValidNumber(value)) {
            setDepositAmount(value);
        }
    };

    const handleDeposit = () => {
        const signer = library.getSigner();
        const tx = signer.sendTransaction({
            to: PresaleAdress,
            value: ethers.utils.parseEther(depositAmount).toHexString(),
        });
        tryTransact(tx, modalContext.pushMessage)
    };

    return (
        <>
            <span className="m-2 text-4xl text-center">Presale ends in</span>

            <span className="m-2 text-4xl text-center">
                {!presaleConstants.endtime ? 
                    "[Loading...]" :
                <Countdown date={presaleConstants.endtime.toLocaleString()} />
            }
            </span>
            <span className="m-2 text-4xl text-center">
                or after we raise {presaleConstants.hardCap} AVAX.
            </span>

            <span className="m-2 text-4xl">
                We raised{" "}
                {presaleBalance !== null ? printBigNumber(presaleBalance) : ""}{" "}
                AVAX so far.
            </span>
            <div className="w-full p-1 my-8 bg-indigo-500 rounded-md">
                {presaleBalance !== null ? (
                    <div
                        className="py-1 text-xs leading-none text-center text-white bg-indigo-900 rounded-md"
                        style={{ width: printPercentage(presaleBalance) }}
                    >
                        {printPercentage(presaleBalance)}
                    </div>
                ) : null}
            </div>
            <div className="w-full p-4 my-8 bg-indigo-500 rounded-md">
                <h2 className="text-2xl">Deposit AVAX for OPMN</h2>
                <p>{presaleConstants.tokenPerAvax} OPMN per 1 AVAX</p>
                <p>
                    Your AVAX balance:{" "}
                    <span className="font-bold">
                        {avaxBalance !== null
                            ? printBigNumber(avaxBalance)
                            : ""}{" "}
                        AVAX
                    </span>
                </p>
                <input
                    className="w-5/6 p-3 m-3 bg-indigo-500 border-4 border-indigo-900 rounded-md focus:outline-none"
                    type="text"
                    onChange={handleDepositInput}
                    value={depositAmount}
                />
                <button
                    className="p-2 bg-indigo-900 rounded-md focus:outline-none focus:ring-none hover:underline"
                    onClick={handleDeposit}
                >
                    Deposit
                </button>
            </div>
        </>
    );
}
