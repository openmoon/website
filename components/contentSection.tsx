import { ReactNode } from "react";

interface IContentSectionProps {
    title: string;
    bgColor: string;
    children: ReactNode;
}

export default function ContentSection(props: IContentSectionProps) {
    return (
        <section className={`px-6 py-20 bg-${props.bgColor} text-white`}>
            <div className="container mx-auto flex items-center">
                <div className="flex-1">
                    <h2 className="text-2xl sm:text-3xl border-l-4 border-secondary pl-4 mb-4">
                        {props.title}
                    </h2>
                    {props.children}
                </div>
                <div className="hidden lg:flex flex-1 justify-end bg-indigo-700"></div>
            </div>
        </section>
    );
}
