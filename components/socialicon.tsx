import { IconDefinition } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

export interface ISocialIconProps {
    icon: IconDefinition;
    href: string;
}

export default function SocialIcon(props: ISocialIconProps) {
    return (
        <a
            href={props.href}
            target="_blank"
            rel="noopener noreferrer"
            className="flex flex-row justify-center rounded-lg w-8 h-8 p-2 bg-white text-indigo-900 hover:bg-gray-300 "
        >
            <FontAwesomeIcon icon={props.icon} />
        </a>
    );
}
