import Link from "next/link";

interface INavLinkProps {
    title: string;
    href: string;
    selected: boolean;
}

export default function NavLink(props: INavLinkProps) {
    const underline = props.selected ? "underline" : "";
    return (
        <Link href={props.href}>
            <a className={`hover:underline ${underline} py-2 md:px-1`}>
                {props.title}
            </a>
        </Link>
    );
}
