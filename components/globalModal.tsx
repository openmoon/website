import {createContext, ReactNode, useState} from "react";
import ReactModal from 'react-modal'

export interface IGlobalModalProps {
    children: ReactNode
}

export interface IGlobalModalContext {
    pushMessage: (msg: string) => void
}

export const GlobalModalContext = createContext<IGlobalModalContext>(null)
export default function GlobalModal(props: IGlobalModalProps) {
    const [messages, setMessages] = useState<string[]>([])
    const context: IGlobalModalContext = {
        pushMessage: (msg: string) => setMessages([...messages, msg])
    }
    const onDismiss = () => {
        const [msg, ...msgs] = messages
        setMessages(msgs)
    }
    return <GlobalModalContext.Provider value={context}>
        <ReactModal
            isOpen={messages.length != 0}
            onRequestClose={onDismiss}
            ariaHideApp={false}
            className="m-10 focus:outline-none"
        >
            <div
                className="h-auto max-w-3xl p-5 m-auto text-xl text-white bg-indigo-900 rounded-xl">
                <p className="m-2 text-red-400">Error:</p>
                <p className="m-2 text-red-400">{messages[0]}</p>
        <button className="p-2 m-2 bg-indigo-500 rounded-md focus:outline-none focus:ring-none hover:underline" onClick={onDismiss}>
            OK
        </button>


            </div>
        </ReactModal>
        {props.children}
    </GlobalModalContext.Provider>
}
