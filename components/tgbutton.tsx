import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTelegramPlane } from "@fortawesome/free-brands-svg-icons";

export default function TgButton() {
    return (
        <a
            href="https://t.me/OpenMoonCommunity"
            className="flex flew-row items-center justify-center space-x-3 rounded-md bg-blue-500 hover:bg-blue-600 p-2"
        >
            <FontAwesomeIcon icon={faTelegramPlane} size="lg" />
            <span>Join Telegram</span>
        </a>
    );
}
