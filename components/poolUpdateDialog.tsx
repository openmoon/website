import {BigNumber, ethers} from "ethers";
import {isValidNumber} from "./util";
import ReactModal from 'react-modal';
import {PoolData} from "./stakingPool";
import {useState} from "react";


interface IPoolUpdateDialogProps {
    dType: PoolUpdateType,
    poolData: PoolData
    onClose: () => void,
    onStake: (amount: BigNumber) => void,
    onWithdraw: (amount: BigNumber) => void,
    onApprove: () => void,
}

export enum PoolUpdateType {
    Deposit,
    Withdraw,
}
export default function PoolUpdateDialog(props: IPoolUpdateDialogProps) {
    const [input, setInput] = useState<string>();
    const handleInput = (event: any) => {
        const {value} = event.target;
        if (isValidNumber(value)
        ) {
            setInput(value);
        }
    };
    var value: BigNumber;
    try {
        value = ethers.utils.parseEther(input);
    } catch {
        value = null;
    }
    const validateInput = () => {
        if (value == null) {
            return "Invalid number!";
        }
        if (props.dType == PoolUpdateType.Deposit && props.poolData.OPMNBalanceAccount.lt(value)) {
            return "Selected amount exceeds your account's current balance.";
        }
        if (props.dType == PoolUpdateType.Withdraw && props.poolData.stakeAccount.lt(value)) {
            return "Selected amount exceeds your account's current stake.";
        }
        return null;
    }

    const onMax = () => {
        switch (props.dType) {
            case PoolUpdateType.Withdraw:
                setInput(ethers.utils.formatEther(props.poolData.stakeAccount))
                break
            case PoolUpdateType.Deposit:
                setInput(ethers.utils.formatEther(props.poolData.OPMNBalanceAccount))
                break
        }
    }
    return <ReactModal
        isOpen={props.dType != null}
        onRequestClose={props.onClose}
        data={{tabindex: null}}
        ariaHideApp={false}
        className="m-10 focus:outline-none"
    >
        <div
            className="h-auto max-w-3xl p-5 m-auto text-white bg-indigo-900 rounded-xl">
            <h1 className="m-5 text-5xl text-center">
                {props.dType == PoolUpdateType.Deposit ?
                    "Stake OPMN for " + props.poolData.rewardName :
                    "Unstake OPMN from " + props.poolData.rewardName + " pool"}
            </h1>
            <div className="flex m-2 space-x-2">
                <input
                    className="flex-grow float-right w-auto w-full p-3 bg-indigo-500 rounded-md focus:outline-none"
                    type="text"
                    value={input}
                    onChange={handleInput}
                />
                <button className="p-2 ml-auto mr-0 bg-indigo-500 rounded-md focus:outline-none focus:ring-none hover:underline disabled:text-darkBack disabled:opacity-50"
                    onClick={onMax}
                >
                    MAX
                </button>
            </div>
            <p className="mx-2 text-red-600">{validateInput()}</p>
            {props.dType == PoolUpdateType.Deposit ?
                value == null || props.poolData.OPMNAllowance.gt(value) ?
                    <button className="p-2 m-2 bg-indigo-500 rounded-md focus:outline-none focus:ring-none hover:underline disabled:text-darkBack disabled:opacity-50"
                        onClick={() => props.onStake(value)}
                        disabled={validateInput() != null}>
                        Stake
                    </button> :
                    <button className="p-2 m-2 bg-indigo-500 rounded-md focus:outline-none focus:ring-none hover:underline disabled:text-darkBack disabled:opacity-50"
                        onClick={props.onApprove}
                        disabled={validateInput() != null}>
                        Approve
                    </button> :
                <button className="p-2 m-2 bg-indigo-500 rounded-md focus:outline-none focus:ring-none hover:underline disabled:text-darkBack disabled:opacity-50"
                    onClick={() => props.onWithdraw(value)}
                    disabled={validateInput() != null}>
                    Unstake
                </button>}
            <button className="p-2 m-2 bg-indigo-500 rounded-md focus:outline-none focus:ring-none hover:underline"
                onClick={props.onClose}>
                Cancel
            </button>
        </div>
    </ReactModal>
}
