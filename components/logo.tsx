import Image from "next/image";

export default function Logo() {
    return (
        <a className="text-3xl flex flex-row items-center space-x-3" href="#">
            <Image
                src="/logo.png"
                alt="OpenMoon.finance"
                width="48"
                height="48"
            ></Image>
            <span>OpenMoon.finance</span>
        </a>
    );
}
