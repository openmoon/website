import {useWeb3React} from "@web3-react/core"
import {ReactNode, useEffect} from "react"
import {ChainID} from "./consts"
import ReactModal from 'react-modal'
import {Web3Provider} from "@ethersproject/providers"
import {Connector} from './util'


export interface INetworkWrapperProps {
    children: ReactNode
    showModal: boolean
}

export default function NetworkWrapper(props: INetworkWrapperProps) {
    const {account, library, chainId, active, activate} = useWeb3React<Web3Provider>()
    useEffect(() => {
        if (!active)
            activate(Connector)
    }, [account])
    if (!!account &&
        !!library &&
        chainId == ChainID) {
        return <>
            {props.children}
        </>
    }
    else if (props.showModal) {
        return <ReactModal
            isOpen={true}
            ariaHideApp={false}
            className="m-10 focus:outline-none"
        >
            <div
                className="h-auto max-w-3xl p-5 m-auto text-xl text-white bg-indigo-900 rounded-xl">
                <p className="m-2 text-red-400">
                    {(() => {
                        if (active) {
                            return "Invalid Network selected!"
                        }
                        else{
                            return "Connecting to web3..."
                        }
                    })()}
                </p>
            </div>
        </ReactModal>
    }
    else return <></>
}
