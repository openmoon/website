import {useWeb3React} from "@web3-react/core";
import {BigNumber, ethers} from "ethers";
import {useContext, useEffect, useState} from "react";
import {ERC20ABI, StakingABI, StakingAddress, TokenAdress} from "./consts";
import {bigNumberToInt, printBigNumber, tryTransact} from "./util";
import ReactModal from 'react-modal';
import {PoolUpdateType} from './poolUpdateDialog'
import PoolUpdateDialog from './poolUpdateDialog'
import {GlobalModalContext, IGlobalModalContext} from "./globalModal";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";


export interface IStakingPoolProps {
    reward: string,
    name?: string,
    onDelete?: () => void,
}

export interface PoolData {
    rewardName: string
    rewardSymbol: string
    OPMNBalanceAccount: BigNumber
    OPMNAllowance: BigNumber
    stakeAccount: BigNumber
    stakeTotal: BigNumber
    rewardAvailable: BigNumber
    rewardPending: BigNumber
}

export default function StakingPool(props: IStakingPoolProps) {

    const {account, library, chainId} = useWeb3React();
    const [poolData, setPoolData] = useState<PoolData>(null);
    const [dialog, setDialog] = useState<PoolUpdateType>(null);


    const stakingContract = new ethers.Contract(
        StakingAddress,
        StakingABI,
        library?.getUncheckedSigner(),
    );

    const tokenContract = new ethers.Contract(
        TokenAdress,
        ERC20ABI,
        library?.getUncheckedSigner(),
    );

    const setState = () => {
        (async function () {
            const data = await stakingContract.poolUserInfo(props.reward, account);
            setPoolData(data);
        })();
    }

    useEffect(() => {
        {
            setState();
            library.on("block", setState);
        }
    }, [account, chainId, library]);

    const modalContext: IGlobalModalContext = useContext(GlobalModalContext)

    const onStake = (value: BigNumber) => {
        setDialog(null)
        const tx = stakingContract.deposit(props.reward, value);
        tryTransact(tx, modalContext.pushMessage)
    }

    const onApprove = () => {
        setDialog(null)
        const tx = tokenContract.approve(StakingAddress, "115792089237316195423570985008687907853269984665640564039457584007913129639935");
        tryTransact(tx, modalContext.pushMessage)

    }

    const onWithdraw = (value: BigNumber) => {
        setDialog(null)
        const tx = stakingContract.withdraw(props.reward, value);
        tryTransact(tx, modalContext.pushMessage)
    }
    return <>
        {dialog != null &&
            <PoolUpdateDialog
                dType={dialog}
                poolData={poolData}
                onClose={() => {setDialog(null)}}
                onStake={onStake}
                onWithdraw={onWithdraw}
                onApprove={onApprove}
            />
        }
        <div className="p-4 bg-indigo-500 rounded-md">
            <div className="text-3xl">
                {props.name ?? poolData?.rewardName ?? "[Loading...]"}
        {!!props.onDelete ?
                    <button className="float-right p-2 text-3xl text-red-600 rounded-md focus:outline-none" onClick={props.onDelete}>
                        <FontAwesomeIcon icon={faTimes} onClick={props.onDelete}/>
                    </button>
                    : null
                }
            </div>

            <p className="text-xs">
                ({props.reward})
            </p>
            <br />
            {!!poolData ? <>
                <p className="text-2xl">
                    User's stake: {printBigNumber(poolData.stakeAccount)} OPMN
                </p>
                <p className="text-2xl">
                    Total stake: {printBigNumber(poolData.stakeTotal)} OPMN
                </p>
                <p className="text-2xl">
                    Available reward: {printBigNumber(poolData.rewardAvailable)} {poolData.rewardSymbol}
                </p>
                <p className="text-2xl">
                    Pending reward: {printBigNumber(poolData.rewardPending)} {poolData.rewardSymbol}
                </p>
                {
                    poolData.stakeTotal.gt(BigNumber.from(0)) ?
                        <p className="text-2xl">
                            Est. reward after 24h: {printBigNumber(poolData.rewardAvailable.mul(poolData.stakeAccount).div(poolData.stakeTotal).div(BigNumber.from(50)))} {poolData.rewardSymbol}
                        </p>
                        : null
                }
            </>
                :
                null
            }
            <br />
            <div className="space-x-2">
                <button className="p-2 text-3xl bg-indigo-900 rounded-md focus:outline-none focus:ring-none hover:underline" onClick={() => setDialog(PoolUpdateType.Deposit)}>
                    Deposit
                </button>
                <button className="p-2 text-3xl bg-indigo-900 rounded-md focus:outline-none focus:ring-none hover:underline" onClick={() => setDialog(PoolUpdateType.Withdraw)}>
                    Withdraw
                </button>
                <button className="p-2 text-3xl bg-indigo-900 rounded-md focus:outline-none focus:ring-none hover:underline" onClick={() => onWithdraw(BigNumber.from(0))}>
                    Harvest
                </button>
                
            </div>
        </div>
    </>
}
