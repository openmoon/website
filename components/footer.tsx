import {
    faGitlab,
    faTelegramPlane,
    faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import Logo from "./logo";
import SocialIcon, { ISocialIconProps } from "./socialicon";

export default function Footer() {
    const socials: Array<ISocialIconProps> = [
        {
            href: "https://t.me/OpenMoonCommunity",
            icon: faTelegramPlane,
        },
        {
            href: "https://twitter.com/OpenmoonAVAX",
            icon: faTwitter,
        },
        {
            href: "https://gitlab.com/openmoon",
            icon: faGitlab,
        },
    ];
    return (
        <footer className="flex flew-row flex-cols-3 justify-center text-white space-x-10 mt-12 pb-6">
            <div></div>
            <div className="max-w-screen-xl w-full flex flex-col md:flex-row md:justify-between">
                <Logo />
                <div className="py-6">
                    <span>Stay connected:</span>
                    <div className="flex flex-row mt-2 space-x-2">
                        {socials.map((s) => (
                            <SocialIcon
                                key={s.href}
                                href={s.href}
                                icon={s.icon}
                            />
                        ))}
                    </div>
                </div>
            </div>
            <div></div>
        </footer>
    );
}
