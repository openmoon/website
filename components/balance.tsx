export interface IBalanceProps {
    adress: string;
    balance: string;
}

export default function Balance(props: IBalanceProps) {
    return (
        <>
            <div className="flex flew-row items-center justify-center space-x-3 rounded-md bg-indigo-500  p-1">
                <span>{props.balance} OPMN</span>
                <button className="rounded-md bg-indigo-900 p-2 focus:outline-none focus:ring-none hover:underline">
                    {props.adress}
                </button>
            </div>
        </>
    );
}
